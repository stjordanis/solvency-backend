package app

import (
	"net/http"

	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/api"
)

type Server struct {
	s   *api.Server
	svc Service
}

func Init(svc Service) Server {
	s := Server{s: new(api.Server), svc: svc}
	*s.s = api.InitServer()

	r := s.s.Router
	r.Get("/data/latest_snapshot", fetchLatestSnapshotAssetData(svc))

	return s
}

func (s *Server) Router() http.Handler {
	return s.s.Router
}

func (s *Server) Close() {}
