package app

import (
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type Service interface {
	FetchLatestAssetDataBySymbol(sym string) (*t.AssetData, error)
	FetchAllSupportedAssetSymbols() ([]string, error)
}
