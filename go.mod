module gitlab.com/thorchain/devops/solvency-app/solvency-backend

go 1.13

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.2.0
	github.com/go-chi/render v1.0.1
	github.com/gorilla/schema v1.2.0
	github.com/graphql-go/graphql v0.7.9
	github.com/joho/godotenv v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.8.1
)
