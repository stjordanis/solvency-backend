package mongo

import (
	"crypto/tls"
	"fmt"
	"net"
	"strings"

	m "github.com/globalsign/mgo"
)

type Store struct {
	sess   *m.Session
	dbName string
}

func InitStore(mongoDBName, mongoHost, mongoPwd, mongoUser string) (s Store, err error) {
	s.dbName = mongoDBName
	s.sess, err = newClient(mongoHost, mongoUser, mongoPwd)

	return
}

func (s *Store) Close() {
	s.sess.Close()
}

func (s *Store) DB() *m.Database {
	return s.sess.Copy().DB(s.dbName)
}

func (s *Store) DBName() string {
	return s.dbName
}

func (s *Store) C(colName string) (*m.Session, *m.Collection) {
	sess := s.sess.Copy()
	return sess, sess.DB(s.dbName).C(colName)
}

func newClient(host, user, pwd string) (*m.Session, error) {
	if user == "" {
		return m.Dial(host)
	}
	hostParts := strings.Split(host, "-")
	hostPre := hostParts[0]
	hostSuff := hostParts[1]
	hosts := []string{
		fmt.Sprintf("%s-shard-00-00-%s:27017", hostPre, hostSuff),
		fmt.Sprintf("%s-shard-00-01-%s:27017", hostPre, hostSuff),
		fmt.Sprintf("%s-shard-00-02-%s:27017", hostPre, hostSuff),
	}

	dialInfo := &m.DialInfo{
		Addrs:    hosts,
		Username: user,
		Password: pwd,
	}
	tlsConfig := &tls.Config{}
	dialInfo.DialServer = func(addr *m.ServerAddr) (net.Conn, error) {
		conn, err := tls.Dial("tcp", addr.String(), tlsConfig)
		return conn, err
	}
	return m.DialWithInfo(dialInfo)
}
