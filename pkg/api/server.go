package api

import (
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/render"
	"github.com/sirupsen/logrus"
)

type Server struct {
	logger *StandardLogger
	Router *chi.Mux
	port   string
}

func InitServer() Server {
	r := chi.NewRouter()
	r.Use(
		render.SetContentType(render.ContentTypeJSON),
		middleware.Logger,
		middleware.RealIP,
		middleware.RequestID,
		middleware.Timeout(60*time.Second),
		Recoverer,
	)
	lg := initLogger()

	return Server{
		logger: &lg,
		Router: r,
	}
}

func (s *Server) Start(port string) {
	s.logger.httpInfo(port)
	if err := http.ListenAndServe(":"+port, s.Router); err != nil {
		s.logger.HttpError(port, err.Error())
	}
}

// Event stores messages to log later, from our standard interface
type logEvent struct {
	id      int
	message string
}

// StandardLogger enforces specific log message formats
type StandardLogger struct {
	*logrus.Logger
}

func initLogger() StandardLogger {
	stdLogger := StandardLogger{logrus.New()}
	stdLogger.Formatter = &logrus.JSONFormatter{}

	return stdLogger
}

var (
	httpErrMessage  = logEvent{1, "An error occured starting HTTP listener at port: %s. Error: %s"}
	httpInfoMessage = logEvent{2, "Starting HTTP service at %s"}
)

func (l *StandardLogger) httpInfo(port string) {
	l.Infof(httpInfoMessage.message, port)
}

func (l *StandardLogger) HttpError(port string, err string) {
	l.Errorf(httpErrMessage.message, port, err)
}
