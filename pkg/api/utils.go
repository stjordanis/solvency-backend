package api

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/gorilla/schema"
)

func URLParam(r *http.Request, param string) string {
	return chi.URLParam(r, param)
}

var decoder = schema.NewDecoder()

func WriteJSON(w http.ResponseWriter, code int, payload interface{}) {
	b, err := json.Marshal(payload)
	if err != nil {
		panic(Error{http.StatusInternalServerError, err})
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(b)
}

type Validator interface {
	Validate() error
}

func ParseAndValidate(r *http.Request, out Validator) {
	parseHTTPParams(r, out)
	if err := out.Validate(); err != nil {
		Abort(http.StatusUnprocessableEntity, err)
	}
}

type ParseHTTPParamsPostProcessor interface {
	PostProcess()
}

func parseHTTPParams(r *http.Request, out Validator) {
	if r.Method == "GET" {
		if err := decoder.Decode(out, r.URL.Query()); err != nil {
			Abort(http.StatusUnprocessableEntity, err)
		}
		if params, ok := out.(ParseHTTPParamsPostProcessor); ok {
			params.PostProcess()
		}
		return
	}
	var b bytes.Buffer
	bodyCpy := io.TeeReader(r.Body, &b)
	if err := json.NewDecoder(bodyCpy).Decode(out); err != nil {
		Abort(http.StatusUnprocessableEntity, err)
	}
	r.Body = ioutil.NopCloser(&b)
	if params, ok := out.(ParseHTTPParamsPostProcessor); ok {
		params.PostProcess()
	}
}
