package main

import (
	"flag"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/go-chi/cors"
	"github.com/joho/godotenv"

	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/impl"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/api"
	m "gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/mongo"
	sched "gitlab.com/thorchain/devops/solvency-app/solvency-backend/scheduler"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/task"
	"gitlab.com/thorchain/devops/solvency-app/solvency-backend/v1/app"
)

// wire up the app server and snapshot scheduler
func main() {
	envPath := flag.String("env", ".env", "path for environment variables")
	flag.Parse()
	if err := godotenv.Load(*envPath); err != nil {
		log.Fatalln("Error loading environment variables: ", err)
	}

	mongoHost := os.Getenv("MONGO_HOST")
	mongoUser := os.Getenv("MONGO_USER")
	mongoPwd := os.Getenv("MONGO_PWD")
	mongoDBName := os.Getenv("MONGO_DB_NAME")

	store, err := m.InitStore(mongoDBName, mongoHost, mongoPwd, mongoUser)
	checkError(err)

	appServer := initServer(store)
	sc := initFetchSolvencyDataScheduler(store)
	go sc.Start()
	defer cleanup(&appServer, &sc)

	apiServer := api.InitServer()
	apiServer.Router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "PATCH", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	}).Handler)
	apiServer.Router.Mount("/api/v1/solvency", appServer.Router())

	apiServer.Start(os.Getenv("PORT"))
}

func initServer(store m.Store) app.Server {
	return app.Init(&impl.Service{Repo: &impl.DB{Store: &store}})
}

func initFetchSolvencyDataScheduler(store m.Store) sched.Scheduler {
	host := os.Getenv("THOR_NODE_HOST")
	port := os.Getenv("THOR_NODE_PORT")
	secs, err := strconv.Atoi(os.Getenv("SNAP_SHOT_INTERVAL_SECONDS"))
	checkError(err)

	return sched.Init(
		time.NewTicker(time.Second*time.Duration(secs)),
		&task.SolvencyDataFetcher{
			&impl.Service{Repo: &impl.DB{&store}},
			&impl.ThorNodeClient{host, port},
		},
	)
}

func checkError(err error) {
	if err != nil {
		log.Fatalln("Error: ", err)
	}
}

type Closer interface {
	Close()
}

func cleanup(closers ...Closer) {
	for _, c := range closers {
		c.Close()
	}
}
