package impl

import (
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type Service struct {
	Repo Repository
}

func (s *Service) FetchLatestAssetDataBySymbol(sym string) (*t.AssetData, error) {
	return s.Repo.FindLatestAssetDataBySymbol(sym)
}

func (s *Service) FetchAllSupportedAssetSymbols() ([]string, error) {
	return s.Repo.FindAllSupportedAssetSymbols()
}

type Repository interface {
	FindAllSupportedAssetSymbols() ([]string, error)
	FindLatestAssetDataBySymbol(sym string) (*t.AssetData, error)
}
