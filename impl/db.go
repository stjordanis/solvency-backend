package impl

import (
	b64 "encoding/base64"
	"time"

	m "gitlab.com/thorchain/devops/solvency-app/solvency-backend/pkg/mongo"
	t "gitlab.com/thorchain/devops/solvency-app/solvency-backend/types"
)

type DB struct {
	*m.Store
}

// SupportedAsset methods

const supportedAssetsColl = "supported_assets"

func (db *DB) FindAllSupportedAssetSymbols() ([]string, error) {
	sess, c := db.C(supportedAssetsColl)
	defer sess.Close()

	var assets []t.SupportedAsset
	err := m.Find(c, &assets, nil)
	if err != nil {
		return nil, err
	}

	symbols := make([]string, len(assets))
	for i := range assets {
		sym, err := b64.StdEncoding.DecodeString(assets[i].Base64EncodedSymbol)
		if err != nil {
			return nil, err
		}
		symbols[i] = string(sym)
	}

	return symbols, nil
}

func (db *DB) UpsertSupportedAsset(in *t.SupportedAsset) (out *t.SupportedAsset, err error) {
	out = &t.SupportedAsset{}
	sess, c := db.C(supportedAssetsColl)
	defer sess.Close()

	in.Base64EncodedSymbol = b64.StdEncoding.EncodeToString([]byte(in.Symbol))
	err = m.Upsert(c, out, m.M{"_id": in.Base64EncodedSymbol}, m.M{"$set": in})

	return
}

// AssetData methods

const assetDataColl = "asset_data"

func (db *DB) FindAllAssetDataBySymbol(symbol string) (out *t.AssetData, err error) {
	out = &t.AssetData{}
	sess, c := db.C(assetDataColl)
	defer sess.Close()

	id := b64.StdEncoding.EncodeToString([]byte(symbol))
	err = m.Find(c, out, m.M{"_id": id})

	return
}

func (db *DB) FindLatestAssetDataBySymbol(symbol string) (out *t.AssetData, err error) {
	out = &t.AssetData{}
	sess, c := db.C(assetDataColl)
	defer sess.Close()

	id := b64.StdEncoding.EncodeToString([]byte(symbol))
	err = m.FindOne(c, out, m.M{"_id": id}, nil, []string{"-snapshotDate"})

	return
}

func (db *DB) SaveAssetData(in *t.AssetData) (out *t.AssetData, err error) {
	out = &t.AssetData{}
	sess, c := db.C(assetDataColl)
	defer sess.Close()

	in.CreatedAt = time.Now().UTC()
	in.Base64EncodedSymbol = b64.StdEncoding.EncodeToString([]byte(in.Symbol))

	err = m.Insert(c, out, in)
	return
}
