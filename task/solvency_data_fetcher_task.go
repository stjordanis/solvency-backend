package task

type Service interface{}

type ThorNodeClient interface{}

type SolvencyDataFetcher struct {
	Service Service
	C       ThorNodeClient
}

func (s *SolvencyDataFetcher) Run() {}

func (s *SolvencyDataFetcher) Kill() {}
